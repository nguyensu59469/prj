<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
      'address',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function hasRoles($rolesName)
    {
        return $this->roles->contains('name', $rolesName);
    }

    public function hasPermission($permissionName)
    {
        $roles = $this->roles;
        foreach ($roles as $role) {
            if ($role->hasPermissions($permissionName)) {
                return true;
            }
        }
        return false;
    }

    public function isSupperAdmin()
    {
        return $this->hasRoles('super-admin');
    }

    public function attachRole($roleId)
    {
        return $this->roles()->attach($roleId);
    }

    public function syncRole($roleId)
    {
        return $this->roles()->sync($roleId);
    }

    public function detachRole()
    {
        return $this->roles()->detach();
    }

    public function scopeSearchWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%' . $name . '%') : null;
    }

    public function scopeSearchWithEmail($query, $email)
    {
        return $email ? $query->where('email', $email) : null;
    }

    public function scopeSearchWithRoleId($query, $roleId)
    {
        return $roleId ? $query->whereHas('roles', fn( $q) => $q->where('role_id', $roleId)) : null;
    }
}
