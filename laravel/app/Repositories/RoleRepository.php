<?php

namespace App\Repositories;
use App\Models\Role;

class RoleRepository extends BaseRepository
{
    public function model()
    {
        return Role::class;
    }
    public function count()
    {
        return $this->model->count();
    }

    public function getPermissionsByGroup($id)
    {
        $role = $this->findOrFail($id);
        $group_permission = $role->permissions->groupBy('group_name');
        return $group_permission;
    }

}
