<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{
    public function model()
    {
        return User::class;
    }

    public function count()
    {
        return $this->model->count();
    }

    public function search($dataSearch)
    {
        return $this->model->searchWithName($dataSearch['name'])->searchWithEmail($dataSearch['email'])
            ->searchWithRoleId($dataSearch['role_id'])->latest('id')->paginate(5);
    }
}
