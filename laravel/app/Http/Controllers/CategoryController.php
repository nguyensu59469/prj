<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\CreateCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;
use App\Models\Category;
use App\Services\CategoryService;

class CategoryController extends Controller
{

    protected  $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $listCategories = $this->categoryService->all();
        return view("admin.categories.index", compact('listCategories'));
    }

    public function show($id)
    {
        $category = $this->categoryService->findOrFail($id);
        return view("admin.categories.show", compact('category'));
    }

    public function create()
    {
        $category = $this->categoryService->all();
        return view("admin.categories.create",  compact('category'));
    }

    public function store(CreateCategoryRequest $request)
    {
        $this->categoryService->store($request);

        return redirect()->route('categories.index')->with('message', "Create Category Successfully!.");
    }


    public function edit($id)
    {
        $category = $this->categoryService->findOrFail($id);
        return view("admin.categories.edit", compact('category'));
    }

    public function update(UpdateCategoryRequest $request, $id)
    {
        $this->categoryService->update($request, $id);
        return redirect()->route("categories.index")->with("message", "Update Category Successfully!");
    }

    public function destroy($id)
    {
        $this->categoryService->destroy($id);
        return redirect()->route("categories.index")->with("message", "Delete Category Successfully!");
    }
}
