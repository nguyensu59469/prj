<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;

class UserController extends Controller
{
    protected $userService;
    protected $roleService;

    public function __construct(RoleService $roleService, UserService $userService)
    {
        $this->roleService = $roleService;
        $this->userService = $userService;
        View::share('role', $this->roleService->all());
    }

    public function index(Request $request)
    {
        $users = $this->userService->search($request);
        return view("admin.users.index", compact('users'));
    }

    public function show($id)
    {
        $user = $this->userService->findById($id);
        return view("admin.users.show", compact('user'));
    }

    public function create()
    {
        $roles = $this->roleService->all();
        return view("admin.users.create",compact('roles'));
    }

    public function store(CreateUserRequest $request)
    {
        $this->userService->store($request);

        $dataCreate = $request->all();
        $dataCreate['password'] = Hash::make($request->password);

//        $user = $this->user->create($dataCreate);
        return redirect()->route('users.index')->with("message", "Create User Successfully!.");
    }

    public function edit($id)
    {
        $user = $this->userService->findById($id);
        return view("admin.users.edit", compact('user'));
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $this->userService->update($request, $id);
        return redirect()->route("users.index")->with("message", "Update User Successfully!.");
    }

    public function destroy($id)
    {
        $this->userService->destroy($id);
        return redirect()->back()->with('message', "Delete User Successfully!.");
    }
}
