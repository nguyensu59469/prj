<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\UpdateProductRequest;
use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Resources\Product\ProductResource;
use App\Services\CategoryService;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    protected $productService;
    protected $categoryService;

    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $categories = $this->categoryService->all();
//        dd($categories);
        $products= $this->productService->all();
        return view("admin.products.index",compact('products','categories'));
    }

    public function list(Request $request)
    {
        $products = $this->productService->search($request);
        return view("admin.products.list", compact('products'));
    }

    public function show($id)
    {

        $product = $this->productService->findById($id);
       return response()->json([
        'data' => $product,
//        'message' => $message
    ], 200);
    }

    public function create()
    {
        $categories = $this->categoryService->all();
        $product = $this->productService->all();
        return view("admin.products.create", compact('product', 'categories'));//sửa ở đây đê dm!
    }

    public function store(CreateProductRequest $request)
    {
        $product = $this->productService->store($request);
        $productResource = new ProductResource($product);
        return $this->sentSuccessResponse($productResource, 'Create Product success.', Response::HTTP_CREATED);
    }

    public function edit($id)
    {
        $product = $this->productService->findById($id);
        return view("admin.products.edit", compact('product'));
    }

    public function update(UpdateProductRequest $request, $id)
    {
        $product = $this->productService->update($request, $id);
        return $this->sentSuccessResponse($product, 'Update product success.', Response::HTTP_OK);
    }

    public function destroy($id)
    {
        $this->productService->destroy($id);
        return $this->sentSuccessResponse('', 'Delete product success.', Response::HTTP_OK);
    }
}
