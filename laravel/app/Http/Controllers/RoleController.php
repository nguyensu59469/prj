<?php

namespace App\Http\Controllers;

use App\Http\Requests\Role\CreateRoleRequest;
use App\Http\Requests\Role\UpdateRoleRequest;
use App\Models\Permission;
use App\Models\Role;
use App\Services\RoleService;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    protected $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    public function index()
    {
//        $roles = $this->roleService->all();
        $roles = Role::latest('id')->paginate(5);
        return view("admin.roles.index", compact('roles'));
    }

    public function show($id)
    {
        $role = $this->roleService->findById($id);
        return view("admin.roles.show", compact('role'));
    }

//    /**
//     *show the form creating a new resource.
//     *
//     *@return  \Illuminate\Http\Response
//     */


    public function create()
    {
        $permission = Permission::all()->groupBy('group_name');
//        dd($permission);
        return view("admin.roles.create", compact('permission'));
    }

    public function store(CreateRoleRequest $request)
    {

        $this->roleService->store($request);
        return redirect()->route('roles.index')->with('message', "Create role successfully.");
    }

    public function edit($id)
    {
        $role = $this->roleService->findById($id);
        $group_permission = $this->roleService->getPermissionsByGroup($id);

        return view("admin.roles.edit", compact('role','group_permission'));
    }

    public function update(UpdateRoleRequest $request, $id)
    {
        $this->roleService->update($request, $id);
        return redirect()->route("roles.index")->with("message", "Update role successfully!");
    }

    public function destroy($id)
    {
        $this->roleService->destroy($id);
        return redirect()->back()->with("message", "Delete role successfully.");
    }
}
