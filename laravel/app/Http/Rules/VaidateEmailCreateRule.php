<?php

namespace App\Http\Rules;

use Illuminate\Contracts\Validation\Rule;

class VaidateEmailCreateRule implements Rule
{

    protected $name;
    protected $email = '@deha-soft.com';

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
//        dd($this->email);
        return preg_match(("/^[A-Za-z0-9_.]{6,32} . $this->email/"), $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute is invalid or already in use.';
    }
}
