<?php

namespace App\Http\Requests\User;

use App\Rules\VaidateEmailUpateRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required',
            'email' => [
                'bail',
                'required',

            ],
            'password' => 'nullable',
            'address' => 'bail|required',
            'phone' => 'bail|required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name cannot be blank.',
            'email.required' => 'Email cannot be blank.',
            'email.unique' => 'Invalid email address or email already in use.',
            'password.required' => 'Password cannot be blank.',
            'phone.required' => 'Phone cannot be null.',
        ];
    }
}
