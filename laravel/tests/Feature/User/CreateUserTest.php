<?php

namespace Tests\Feature\User;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    use WithFaker;

    /** @test */
    public function authenticated_super_admin_can_create_new_user()
    {
        $this->loginWithSuperAdmin();
        $dataCreate = User::factory()->create()->toArray();
        $response = $this->post($this->getStoreUserRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas("users", $dataCreate);
//        $response->assertRedirect($this->getUserRoute());
    }

    /** @test */
    public function super_admin_can_not_create_new_user_if_name_email_phone_address_and_password_is_null()
    {
        $this->loginWithSuperAdmin();
        $data = User::factory()->make([
            'name' => null,
            'email' => null,
            'password' => null,
            'phone' => null,
            'address' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['name'])
            ->assertSessionHasErrors(['email'])
            ->assertSessionHasErrors(['password'])
            ->assertSessionHasErrors(['phone'])
            ->assertSessionHasErrors(['address']);
    }

    /** @test */
    public function super_admin_can_not_create_new_user_if_name_null()
    {
        $this->loginWithSuperAdmin();
        $data = User::factory()->make([
            'name' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function super_admin_can_not_create_new_user_if_email_null()
    {
        $this->loginWithSuperAdmin();
        $data = User::factory()->make([
            'email' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function super_admin_can_not_create_new_role_if_address_null()
    {
        $this->loginWithSuperAdmin();
        $data = User::factory()->make([
            'address' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['address']);
    }

    /** @test */
    public function super_admin_can_not_create_new_role_if_phone_null()
    {
        $this->loginWithSuperAdmin();
        $data = User::factory()->make([
            'phone' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['phone']);
    }

    /** @test */
    public function super_admin_can_not_create_new_role_if_password_null()
    {
        $this->loginWithSuperAdmin();
        $data = User::factory()->make([
            'password' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['password']);
    }

    /** @test */
    public function super_admin_can_see_text_error_create_user_if_name_null()
    {
        $this->loginWithSuperAdmin();
        $data = User::factory()->make([
            'name' => null,
        ])->toArray();
        $response = $this->from($this->getCreateUserRoute())->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function super_admin_can_see_text_error_create_user_if_email_null()
    {
        $this->loginWithSuperAdmin();
        $data = User::factory()->make([
            'email' => null,
        ])->toArray();
        $response = $this->from($this->getCreateUserRoute())->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function super_admin_can_see_text_error_create_user_if_address_null()
    {
        $this->loginWithSuperAdmin();
        $data = User::factory()->make([
            'address' => null,
        ])->toArray();
        $response = $this->from($this->getCreateUserRoute())->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['address']);
    }


    /** @test */
    public function super_admin_can_see_text_error_create_user_if_phone_null()
    {
        $this->loginWithSuperAdmin();
        $data = User::factory()->make([
            'phone' => null,
        ])->toArray();
        $response = $this->from($this->getCreateUserRoute())->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['phone']);
    }

    /** @test */
    public function super_admin_can_see_text_error_create_user_if_password_null()
    {
        $this->loginWithSuperAdmin();
        $data = User::factory()->make([
            'password' => null,
        ])->toArray();
        $response = $this->from($this->getCreateUserRoute())->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['password']);
    }


    /** @test */
    public function authenticated_user_have_permission_can_see_create_user_form()
    {
        $this->loginUserWithPermission('user_create');
        $response = $this->get($this->getCreateUserRoute());

        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('admin.users.create');
    }

    /** @test */
    public function authenticated_user_have_permission_can_create_user()
    {
        $this->loginUserWithPermission('user_create');
        $dataCreate = User::factory()->create()->toArray();
        $response = $this->post($this->getStoreUserRoute(), $dataCreate);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseHas('users', $dataCreate);
//        $response->assertRedirect(route('users.index'));
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_create_new_user_if_name_email_phone_address_and_password_null()
    {
        $this->loginUserWithPermission('user_create');
        $data = User::factory()->make([
            'name' => null,
            'email' => null,
            'phone' => null,
            'address' => null,
            'password' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['name'])->assertSessionHasErrors(['email'])
            ->assertSessionHasErrors(['password'])
            ->assertSessionHasErrors(['phone'])
            ->assertSessionHasErrors(['address']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_create_new_user_if_name_null()
    {
        $this->loginUserWithPermission('user_create');;
        $data = User::factory()->make([
            'name' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_create_new_user_if_email_null()
    {
        $this->loginUserWithPermission('user_create');;
        $data = User::factory()->make([
            'email' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_create_new_user_if_phone_null()
    {
        $this->loginUserWithPermission('user_create');;
        $data = User::factory()->make([
            'phone' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['phone']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_create_new_user_if_address_null()
    {
        $this->loginUserWithPermission('user_create');;
        $data = User::factory()->make([
            'address' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['address']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_not_create_new_user_if_password_null()
    {
        $this->loginUserWithPermission('user_create');;
        $data = User::factory()->make([
            'password' => null,
        ])->toArray();
        $response = $this->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['password']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_see_text_error_create_user_if_name_null()
    {
        $this->loginUserWithPermission('user_create');
        $data = User::factory()->make([
            'name' => null,
        ])->toArray();
        $response = $this->from($this->getCreateUserRoute())->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_see_text_error_create_user_if_email_null()
    {
        $this->loginUserWithPermission('user_create');
        $data = User::factory()->make([
            'email' => null,
        ])->toArray();
        $response = $this->from($this->getCreateUserRoute())->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['email']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_see_text_error_create_user_if_phone_null()
    {
        $this->loginUserWithPermission('user_create');
        $data = User::factory()->make([
            'phone' => null,
        ])->toArray();
        $response = $this->from($this->getCreateUserRoute())->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['phone']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_see_text_error_create_user_if_address_null()
    {
        $this->loginUserWithPermission('user_create');
        $data = User::factory()->make([
            'address' => null,
        ])->toArray();
        $response = $this->from($this->getCreateUserRoute())->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['address']);
    }

    /** @test */
    public function authenticated_user_have_permission_can_see_text_error_create_user_if_password_null()
    {
        $this->loginUserWithPermission('user_create');
        $data = User::factory()->make([
            'password' => null,
        ])->toArray();
        $response = $this->from($this->getCreateUserRoute())->post($this->getStoreUserRoute(), $data);

        $response->assertSessionHasErrors(['password']);
    }

    /** @test */
    public function authenticated_user_not_have_permission_can_not_see_create_user_form()
    {
        $this->loginWithUser();
        $response = $this->get($this->getCreateUserRoute());

        $response->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_create_user_form()
    {
        $user = User::factory()->create();
        $response = $this->get($this->getCreateUserRoute(),$user->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function unauthenticated_user_can_not_create_user()
    {
        $user = User::factory()->create();
        $response = $this->post($this->getStoreUserRoute(),$user->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    public function _makeFactoryUser()
    {
        return User::factory()->make()->toArray();
    }

    public function getCreateUserRoute()
    {
        return route('users.create');
    }

    public function getStoreUserRoute()
    {
        return route('users.store');
    }

    public function getUserRoute()
    {
        return route('users.index');
    }


}
