<?php

namespace Tests\Feature\Category;

use App\Models\Category;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_and_has_permission_user_can_delete_category()
    {
        $category = Category::factory()->create();
        $this->loginUserWithPermission('category_delete');
        $response = $this->delete($this->getDeleteCategoryRoute($category->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('categories.index'));
    }

    /** @test */
    public function authenticated_super_admin_can_delete_category()
    {
        $category = Category::factory()->create();
        $this->loginUserWithPermission('category_delete');
        $response = $this->deleteJson($this->getDeleteCategoryRoute($category->id));

        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('categories.index'));
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_category()
    {
        $category = Category::factory()->create();
        $response = $this->deleteJson($this->getDeleteCategoryRoute($category->id));

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test */
    public function unauthenticated_super_admin_can_not_delete_category()
    {
        $category = Category::factory()->create();
        $response = $this->deleteJson($this->getDeleteCategoryRoute($category->id));

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function getDeleteCategoryRoute($id)
    {
        return route('categories.destroy', $id);
    }
}
