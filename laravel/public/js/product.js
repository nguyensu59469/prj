
const Product = (function () {
    let modules = {}
    modules.getList = function () {
        let url = $('#show-product').attr('data-action-table');
        var formData = $('#search-product').serialize();
        $.ajax({
            method: 'GET',
            url: url,
            data: formData,
        }).then(function (response) {
            // console.log(response);
            $('#show-product').html('').append(response);
        })
    }

    modules.getListByPage = function (e) {
        let url = e.attr('href');
        Base.callApiNormally(url).then(function (res) {
            $('#show-product').html('').append(res);
        })
    }

    modules.create = function () {
        let url = $('#addProductForm').data('action');
        let data = new FormData(document.getElementById('addProductForm'));
        CustomAlert.resetError();
        Base.callApiData(url, data, 'POST')
            .then(function (res) {
                $('#modal-form-create').modal('hide');
                Product.resetForm();
                Product.getList();
                CustomAlert.messageSuccessModal(res.message);
            })
            .fail(function (res){
                console.log(res)
                CustomAlert.showError(JSON.parse(res.responseText).errors);
            });
    }
    modules.show = function () {
        $('#modal-show').modal('show');
        $('#close-show').click(function () {
            $('#modal-show').modal('hide');
        })
    }


    modules.getShowData = function (element) {
        let url = element.attr('data-url-show');
            Base.callApiNormally(url).then(function (res) {
            // console.log(res);
            $('#product-name').text(res.data.name);
            $('#product-image').attr('src', res.data.image);
            $('#product-quantity').text(res.data.quantity);
            $('#product-price').text(res.data.price);
            $('#product-description').text(res.data.description);

            var categories = "";
            res.data.categories.forEach(category => {
                categories = category.name + "/ ";
                // console.log(categories);
            });
            $('#product-category').text(categories);
            Product.checkImage(res.data.image, 'product-imaged');
        })
    }

    modules.update = function () {
        let url = $('#updateProductForm').data('action');
        // console.log(url);
        let data = new FormData(document.getElementById('updateProductForm'));
        CustomAlert.resetError();
        Base.callApiData(url, data, 'post')
            .done(function (res) {
                Product.resetForm();
                $('#modal-form-update').hide();
                $('.modal-backdrop').remove();
                CustomAlert.messageSuccessModal(res.message);
                Product.getList($('#list').data('action'));
            })
            .fail(function (res){
                console.log(res)
                CustomAlert.showError(JSON.parse(res.responseText).errors);
            });
    }

    modules.delete = function (element) {
        let url = element.attr('data-action');
        console.log(url);
        Base.callApiNormally(url, 'delete').then(function (res){
            Product.getList();
        })
    }


    modules.resetForm = function () {
        $('.selectpicker').val(null).trigger("change");
        $('#addProductForm').trigger('reset');
        $(".show-image").attr("src", "");
    }

    modules.pageLink = function (element) {
        let url = element.attr('href');
        Product.getList(url);
    }

    modules.edit = function (element) {
        let url = element.data('action');
        Product.show(url);
    }

    modules.showModalDetail = function (element) {
        let url = element.data('action');
        Product.getShowData(url);
    }


    modules.checkImage = function (imgSrc, id) {
        var img = new Image();
        img.onload = function () {
            $('#' + id).attr('src', imgSrc);
        };
        img.onerror = function () {
            $('#' + id).attr('src', 'upload/images/default.jpg');
        };
        img.src = imgSrc;
    }


    return modules;
}(window.jQuery, window, document));

$(document).ready(function () {

    Product.getList();

    $('#myForm').on('click', function (e) {
        e.preventDefault();
        CustomAlert.resetError();
        Product.getList();
    })

    $(document).on('click', '.page-link', function (e) {
        e.preventDefault();
        Product.pageLink($(this));
    });

    $(document).on('click', '#btn-create', function (e) {
        e.preventDefault();
        Product.create();
    });

    $(document).on('click', '#btn-add-new-product', function (e) {
        Image.deleteImage();
    });

    $(document).on('change', '.image', function () {
        Image.showImage($(this));
    });

    $(document).on('click', '.btn-delete-image', function (e) {
        e.preventDefault();
        Image.deleteImage();
    });

    $(document).on('click', '.btn-delete', function (e) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                Product.delete($(this));
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
            }
        })
        e.preventDefault();

    });


    $(document).on('click', '.btn-edit-product', function (e) {
        Product.edit($(this));
    });

    $(document).on('click', '#btn-update', function (e) {
        e.preventDefault();
        Product.update();
    });

    $(document).on('click', '.btn-show', function (e) {
        Product.getShowData($(this));
        Product.show();
    });

    $('#category_id').on('change', debounce(function () {
        Product.getList();
    }))

    $('.product_name').on('keyup', debounce(function () {
        Product.getList();
    }))

    $('.min_price, .max_price').on('keyup', debounce(function () {
        Product.getList();
    }))

})

function debounce(func, timeout = 1000) {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, args);
        }, timeout);
    };
}
