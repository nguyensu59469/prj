$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

const Base = (function () {
    let modules = {}

    modules.callApiData = function (url, data = {}, method = 'get') {
        return $.ajax({
            url: url,
            data: data,
            method: method,
            contentType: false,
            processData: false,
        })
    }

    modules.callApiNormally = function (url, data = {}, method = 'get') {
        return $.ajax({
            url: url,
            data: data,
            method: method,
        })
    }

    modules.confirmDelete = function (name) {
        return new Promise((resolve, reject) => {
            Swal.fire({
                title: 'Are you sure delete ' + name + ' ?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    resolve(true);
                } else {
                    reject(false);
                }
            })
        })
    }

    return modules;
}(window.jQuery, window, document));

const Image = (function () {
    let modules = {};

    modules.showImage = function (e) {
        let file = e.get(0).files[0];
        if (file) {
            let reader = new FileReader();
            reader.onload = function () {
                $('.show-image').html('').append('<div class="overlay"> ' +
                    '<button class="text-white bg-danger move-on-hover btn-delete-image" style="z-index: 1">X</button></div>' +
                    '<img src="' + reader.result + '" width="100%">').css('border', '1px solid #eb4b64');
            }
            reader.readAsDataURL(file);
        }
    };

    modules.deleteImage = function () {
        $('.show-image').html('').css('border', '0');
        $('.image').val(null);
    };

    return modules;
}(window.jQuery, window, document));

const CustomAlert = (function (){
    let modules = {}

    modules.messageSuccessModal = function (message) {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: message,
            showConfirmButton: false,
            timer: 1500
        })
    }

    modules.showError = function (obj){
        $.each(obj, function (key,value) {
            $(`.error-${key}`).html(value);
        });
    };

    modules.resetError = function () {
        $('.error').html('');
    };

    modules.showModalError = function (error) {
        Swal.fire({
            icon: 'error',
            title: 'Warning...',
            text: 'Something went wrong!',
        })
    };
    return modules;
}(window.jQuery, window, document));

const Delete = (function (){
    let modules = {}

    modules.delete = function (element) {
        let name = element.data('name-show');
        return new Promise((resolve, reject) => {
            Swal.fire({
                title: 'Are you sure delete ' + name + ' ?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result)=>{
                if (result.isConfirmed) {
                    // alert(element.data('name')+'-'+element.data('id'))
                    $('#delete-'+element.data('name')+'-'+element.data('id')).submit();
                } else {
                    reject(false);
                }
            })
        })
    };
    return modules;
}(window.jQuery, window, document));
