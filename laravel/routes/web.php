<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('roles')->name('roles.')->group(function (){
    Route::get('/',[RoleController::class, 'index'])->name('index');
    Route::post('/',[RoleController::class, 'store'])->name('store');
    Route::get('/create',[RoleController::class, 'create'])->name('create');
    Route::get('/show/{id}',[RoleController::class, 'show'])->name('show');
    Route::get('/edit/{id}',[RoleController::class, 'edit'])->name('edit');
    Route::put('/update/{id}',[RoleController::class, 'update'])->name('update');
    Route::delete('/destroy/{id}',[RoleController::class, 'destroy'])->name('destroy');
});

Route::name('users.')->prefix('users')->group(function (){
    Route::get('/',[UserController::class, 'index'])->name('index');
    Route::post('/',[UserController::class, 'store'])->name('store');
    Route::get('/create',[UserController::class, 'create'])->name('create');
    Route::get('/show/{id}',[UserController::class, 'show'])->name('show');
    Route::get('/edit/{id}',[UserController::class, 'edit'])->name('edit');
    Route::put('/update/{id}',[UserController::class, 'update'])->name('update');
    Route::delete('/destroy/{id}',[UserController::class, 'destroy'])->name('destroy');
});

Route::prefix('categories')->name('categories.')->group(function (){
    Route::get('/',[CategoryController::class, 'index'])->name('index');
    Route::post('/',[CategoryController::class, 'store'])->name('store');
    Route::get('/create',[CategoryController::class, 'create'])->name('create');
    Route::get('/show/{id}',[CategoryController::class, 'show'])->name('show');
    Route::get('/edit/{id}',[CategoryController::class, 'edit'])->name('edit');
    Route::put('/update/{id}',[CategoryController::class, 'update'])->name('update');
    Route::delete('/destroy/{id}',[CategoryController::class, 'destroy'])->name('destroy');
});


Route::prefix('products')->name('products.')->group(function (){
    Route::get('/',[ProductController::class, 'index'])->name('index');
    Route::post('/',[ProductController::class, 'store'])->name('store');
    Route::get('/list',[ProductController::class,'list'])->name('list');
    Route::get('/show/{id}',[ProductController::class, 'show'])->name('show');
    Route::get('/create',[ProductController::class, 'create'])->name('create');
    Route::get('/edit/{id}',[ProductController::class, 'edit'])->name('edit');
    Route::post('/update/{id}',[ProductController::class, 'update'])->name('update');
    Route::delete('/destroy/{id}',[ProductController::class, 'destroy'])->name('destroy');
});
