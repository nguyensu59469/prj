
<div class="container modal fade" id="modal-show">
    <div class="modal-dialog">
        <div class="modal-content">
            <div style="margin: 20px 20px 20px 20px;">
                <div>
                    <h1 id="product-title"> Show Product </h1>
                </div>
                <div class="mb-3">

                </div>
                <div class="panel-group">
                    <div class="mb-2">
                        <div class="panel-heading"><strong>Name</strong></div>
                        <div class="panel-body" id="product-name"></div>
                    </div>
                    <div class="mb-2">
                        <div class="panel-heading"><strong>Price</strong></div>
                        <div class="panel-body" id="product-price"></div>
                    </div>
                    <div class="mb-2">
                        <div class="panel-heading"><strong>Describe</strong></div>

                        <option size="5" class="form-select" name="describe" id="product-description" multiple>
                        </option>

                    </div>
                    <div class="mb-2">
                        <div class="panel-heading"><strong>Quantity</strong></div>
                        <div class="panel-body" id="product-quantity"></div>
                    </div>

                    <div class="mb-2">
                        <div class="panel-heading"><strong>Category</strong></div>
{{--                        @foreach($categories as $category)--}}
{{--                            <option--}}
{{--                                value="{{$category->id}}" {{ request()->category_id == $category->id ? 'selected' : '' }}>{{$category->name}}--}}
{{--                            </option>--}}
{{--                        @endforeach--}}
                        <div class="panel-body" id="product-category"></div>
                    </div>
                    <div class="mb-2">
                        <div class="panel-heading"><strong>Image</strong></div>
                        <div class="panel-body" id="product-imaged"></div>
                        <img class="rounded bg-light image-data img-thumbnail"
                             style="max-width: 70%; height: auto;" id="product-image" src=""/>
                    </div>
                </div>
                <a class="btn btn-secondary" id="close-show" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
</div>
{{--@endsection--}}
