
    <form data-action="{{ route('products.update',$product->id) }}" id="updateProductForm" method="post" enctype="multipart/form-data">
        @csrf
        <div class="mb-3">
            <label>Name</label>
            <input type="text" name="name" class="form-control" value="{{ $product->name }}">
            <div class="text-danger error error-name"></div>
        </div>
        <div class="mb-3">
            <label>Price</label>
            <input type="number" name="price" class="form-control" value="{{ $product->price }}">
            <div class="text-danger error error-price"></div>
        </div>
        <div class="mb-3">
            <label class="form-label">Image</label>
            <input type="file" value="" class="form-control image" name="image">
            <div class="show-image" style="width: 25%">
                <img src="{{ $product->image }}" width="100%">
            </div>
            <div class="text-danger error error-image"></div>
        </div>
        <div class="mb-3">
            <label>Quantity</label>
            <input type="number" name="quantity" class="form-control" value="{{ $product->quantity }}">
            <div class="text-danger error error-quantity"></div>
        </div>
        <div class="mb-3">
            <label>Description</label>
            <input type="text" name="description" class="form-control" value="{{ $product->description }}">
            <div class="text-danger error error-description"></div>
        </div>
        <div class="mb-3 set-height-cate">
            <label>Category</label>
            <select class="selectpicker" name="category_ids[]" multiple>
                @foreach($categories as $category)
                    <option {{$product->categories->contains('id',$category->id) ? 'selected' : ''}} value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
            <div class="text-danger error error-category_ids"></div>
        </div>
        <hr>
        <div class="mb-3 text-center">
            <button class="btn btn-rounded btn-secondary" type="button" data-bs-dismiss="modal">Close</button>
            <button class="btn btn-rounded btn-primary" type="submit" id="btn-update">Update</button>
        </div>
    </form>

    <script>
        $(".selectpicker").selectpicker('refresh');
    </script>


