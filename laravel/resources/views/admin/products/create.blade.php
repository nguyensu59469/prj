{{--@extends('admin.Layouts.dashboard')--}}
{{--@section('content')--}}

    <div id="modal-form-create" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="text-center mt-2 mb-4">
                        <h2 class="page-title text-danger">Create Product</h2>
                        <hr>
                    </div>
                    <form data-action="{{ route('products.store') }}" id="addProductForm" method="POST" class="ps-3 pe-3" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label>Name</label>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Name">
                            <div class="text-danger error error-name"></div>
                        </div>
                        <div class="mb-3">
                            <label>Price</label>
                            <input name="price" id="price" class="form-control" placeholder="Price">
                            <div class="text-danger error error-price"></div>
                        </div>
                        <div class="mb-3">
                            <label>Description</label>
                            <input type="text" name="description" id="description" class="form-control" placeholder="Description">
                            <div class="text-danger error error-description"></div>
                        </div>
                        <div class="mb-3">
                            <label>Quantity</label>
                            <input name="quantity" id="quantity" class="form-control" placeholder="Quantity">
                            <div class="text-danger error error-quantity"></div>
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Image</label>
                            <input type="file" name="image" class="form-control image" id="image">
                            <div class="show-image" style="width: 25%;">

                            </div>
                            <div class="text-danger error error-image"></div>
                        </div>
                        <div class="mb-3 row">
                            <label>Category</label>
                            <select class="selectpicker" name="category_ids[]" multiple data-live-search="true">
                                @foreach($categories  as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                            <div class="text-danger error error-category_ids"></div>
                        </div>
                        <hr>
                        <div class="mb-3 text-center">
                            <button class="btn btn-rounded btn-secondary" type="button" data-bs-dismiss="modal">Close</button>
                            <button class="btn btn-rounded btn-primary" type="submit" id="btn-create">Create</button>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

{{--@endsection--}}

