@extends("admin.Layouts.dashboard")
@section("content")
    <div class="container-fluid">

        <!-- start page title -->
        @if(session('message'))
            <div>
                <h4 class="text-primary">{{ session('message') }}</h4>
            </div>
        @endif

        <div style="padding-left: 1%">
            <div>
                <a href="{{route('users.create')}}" class="btn btn-dribbble">Create User</a>
            </div>
            <h1>User List</h1>

        </div>
        <!-- end page title -->



        <div class=" col-md-10 set-search">
            <form action="" method="get">
                <div class="mb-3" style="display:flex">
                    <label class="form-label">Name&emsp;</label>
                    <input value="{{request()->name ?? ''}}" placeholder="Name" type="text" name="name"
                           class="form-control"
                           style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
                </div>
                <div class="mb-3" style="display:flex">
                    <label class="form-label">Email&emsp;</label>
                    <input value="{{request()->email ?? ''}}" placeholder="@gmail.com" type="text" name="email"
                           class="form-control"
                           style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
                </div>
                <div class="mb-3" style="display: flex">
                    <label class="form-label">Role</label>
                    <select class="form-select" name="category" type="text"
                            style="padding-left: 10px; margin-left: 20px; background-color: #fff; border: 1px solid #e7ebf0;">
                        <option value="" >Select</option>
                        @foreach($role as $item)
                            <option {{ $item->id == request('role_id') ? 'selected' : '' }} value="{{ $item->id }}">
                                {{ $item->display_name }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-dribbble" style="margin-left: 5px">Search</button>
            </form>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">

                            <table class="table table-centered table-bordered w-100 dt-responsive nowrap">
                                <thead class="table-light">
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th style="width: 85px;text-align: center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($users) != 0)
                                    @foreach($users as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ $item->phone }}</td>

                                            <td>
                                                <a href="{{route('users.edit', $item->id)}}" class="btn btn-success">Edit</a>
                                                <a href="{{ route('users.show', $item->id) }}" class="btn btn-warning">Show</a>
                                                <form action="{{ route('users.destroy', $item->id) }}" method="post" style="display: inline">
                                                    @csrf
                                                    @method('Delete')
                                                    <button  class="btn btn-danger btn-delete">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                @else
                                    <tbody>
                                    <tr>
                                        <td colspan="5" class="text-center"><span style="font-size: 25px; color: #d8d8d8">No data...</span></td>
                                    </tr>
                                    </tbody>
                                @endif
                            </table>
                            {{ $users->appends(request()->all())->links() }}
                        </div>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
    </div>
    @push('scripts')
        <script src="{{asset('js/user.js')}}"></script>
    @endpush
@endsection
