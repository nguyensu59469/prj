@extends("admin.Layouts.dashboard")
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Create User</h4>
                </div>
            </div>
        </div>
        <div>
            <form action="{{route('users.store')}}" method="post" enctype="multipart/form-data">
                @csrf

                <div class="input-group input-group-static mb-4">
                    <label>Name</label>
                    <input type="text" value="{{old('name')}}" name="name" class="form-control">

                    @error('name')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Email</label>
                    <input type="text" value="{{old('email')}}" name="email" class="form-control">

                    @error('email')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Phone</label>
                    <input type="int" value="{{old('phone')}}" name="phone" class="form-control">

                    @error('phone')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Address</label>
                    <textarea value="{{old('address')}}" name="address" class="form-control"></textarea>

                    @error('address')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Gender</label>
                    <input type="text" value="{{old('gender')}}" name="gender" class="form-control">
                    <select class="form-control" id="exampleFormControlSelect1">
                        <option value="male">Male</option>
                        <option value="female">Female</option>

                    </select>

                    @error('gender')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror

                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Password</label>
                    <input type="password" value="{{old('password')}}" name="password" class="form-control">

                    @error('password')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror

                </div>
                <div class="mb-3">
                    <label class="form-label">Role</label>
                    <br>
                    <select class="form-select" name="role_ids[]" multiple size="5">
                        @foreach($roles as $role)
                            <option value="{{$role->id}}">{{ $role->display_name }}</option>
                        @endforeach
                    </select>

                </div>
                <div>
                    <a type="button" href="{{ route("users.index") }}" class="btn btn-secondary btn-button">Back</a>
                    <button type="submit" class="btn btn-primary btn-button">Create</button>
                </div>

            </form>
        </div>



{{--        <div class="row">--}}
{{--            <div class="col-12">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-body">--}}
{{--                        <form action="{{ route('users.store') }}" data-action="{{ route('users.store') }}" id="form-add" method="POST" enctype="multipart/form-data">--}}
{{--                            @csrf--}}
{{--                            <div class="modal-body">--}}

{{--                                <div class="mb-3">--}}
{{--                                    <label>Name</label>--}}
{{--                                    <input type="text" name="name" value="{{ old("name") }}" class="form-control" placeholder="Name">--}}
{{--                                </div>--}}
{{--                                <div class="mb-3">--}}
{{--                                    <label>Email address</label>--}}
{{--                                    <input type="email" name="email" value="{{ old("email") }}" class="form-control" placeholder="Email">--}}
{{--                                </div>--}}
{{--                                <div class="mb-3">--}}
{{--                                    <label>Address</label>--}}
{{--                                    <input type="text" name="address" value="{{ old("address") }}" class="form-control" placeholder="Address">--}}
{{--                                </div>--}}

{{--                                <div class="input-group input-group-static mb-4">--}}
{{--                                    <label  name="group" class="ms-0">Group</label>--}}
{{--                                    <select name="gender" class="form-control" >--}}
{{--                                        <option value="male">Male</option>--}}
{{--                                        <option value="fe-male">FeMale</option>--}}

{{--                                    </select>--}}
{{--                                    @error('gender')--}}
{{--                                    <span class="text-danger">{{ $message }}</span>--}}
{{--                                    @enderror--}}
{{--                                </div>--}}

{{--                                <div class="mb-3">--}}
{{--                                    <label>Phone</label>--}}
{{--                                    <input type="number" name="phone" value="{{ old("phone") }}" class="form-control" placeholder="Phone">--}}
{{--                                </div>--}}
{{--                                <div class="mb-3">--}}
{{--                                    <label for="password" class="form-label"> Password</label>--}}
{{--                                    <div class="input-group input-group-merge">--}}
{{--                                        <input type="password" id="password" value="{{ old("password") }}" name="password" class="form-control" placeholder="Enter your password">--}}
{{--                                        <div class="input-group-text" data-password="false">--}}
{{--                                            <span class="password-eye"></span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}


{{--                                --}}{{--                                <div class="mb-3">--}}

{{--                                </div>--}}
{{--                            </div>--}}

{{--                        </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
@endsection

