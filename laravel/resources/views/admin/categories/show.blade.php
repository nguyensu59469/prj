@extends("admin.Layouts.dashboard")
@section("content")
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-md-4" style="margin: auto;padding: 10px;text-align: center">
                <h2 class="page-title">Detail cate {{ $category->name }}</h2>

            </div>
        </div>
        <!-- end page title -->
        <div class="row">
            <div class="col-md-6" style="margin: auto">
                <div class="card">
                    <div class="card-body">
                        <dl class="row">
                            <dt class="col-sm-4">Id :</dt>
                            <dd class="col-sm-8">{{ $category->id }}</dd>
                            <dt class="col-sm-4">Name :</dt>
                            <dd class="col-sm-8">{{ $category->name }}</dd>
                            <dt class="col-sm-4">Slug :</dt>
                            <dd class="col-sm-8">{{ $category->slug }}</dd>
                            @if($category->parent != null)
                                <dt class="col-sm-4">Category parent :</dt>
                                <dd class="col-sm-8">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <span class="badge bg-success">{{ $category->parent->name }}</span>
                                        </div>
                                    </div>
                                </dd>
                            @endif
                        </dl>
                    </div>
                </div>
                <div><br>
                    <a type="button" href="{{ url()->previous() }}" class="btn btn-primary btn-button">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
