@extends('admin.Layouts.dashboard')
@section("content")
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Create Category</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('categories.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">

                                <div class="mb-3">
                                    <label>Name</label>
                                    <input type="text" name="name" value="{{ old("name") }}" class="form-control" placeholder="Name">
                                    @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <label>Category Parent</label>
                                    <select name="parent_id" class="form-control col-sm-4" style="width: 200px">
                                        <option value="0">Category option</option>
                                        @foreach($category as $item)

                                                <option value="{{ $item->id }}" {{ old("parent_id") == $item->id ? "selected" : '' }}>{{ $item->name }}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <a type="button" href="{{ url()->previous() }}" class="btn btn-secondary btn-button">Back</a>
                                <button type="submit" class="btn btn-primary btn-button">Create</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
