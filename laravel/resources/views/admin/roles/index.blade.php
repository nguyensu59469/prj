@extends("admin.Layouts.dashboard")
@section('title', 'Roles')
@section("content")
    <div class="card">
        @if(session('message'))
            <div>
                <h4 class="text-primary">{{ session('message') }}</h4>
            </div>
        @endif
        <div style="padding-left: 1%">
            <h1>Role List</h1>
            <div>
                <a href="{{route('roles.create')}}" class="btn btn-primary">Create</a>
            </div>
        </div>
    </div>
        <div>
            <table class="table table-hover">
                <thead  class="table-light">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Display Name</th>
                        <th>Action</th>

                    </tr>
                </thead>
                @foreach($roles as $role)
                    <tr>
                        <td>{{ $role->id }}</td>
                        <td>{{ $role->name }}</td>

                        <td>{{ $role->display_name }}</td>
                        <td>
                            <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-success">Edit</a>
                            <a href="{{ route('roles.show', $role->id) }}" class="btn btn-warning">Show</a>
                            <form action="{{ route('roles.destroy', $role->id) }}" method="post" style="display: inline">
                                @csrf
                                @method('delete')
                                <button  class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
            {{$roles->links()}}
        </div>

    </div>
@endsection

@push('scripts')
    <script src="{{asset('js/role.js')}}"></script>
@endpush
