@extends("admin.Layouts.dashboard")
@section('title', 'Create Roles')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Create Role</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('roles.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf

                                <div class="input-group input-group-static mb-4">
                                    <label>Name</label>
                                    <input type="text"  value="{{ old('name')}}"  name="name" class="form-control">

                                    @error('name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>

                                <div class="input-group input-group-static mb-4">
                                    <label>Display Name</label>
                                    <input type="text"  value="{{ old('display_name')}}" name="display_name" class="form-control">

                                    @error('display_name')
                                        <span class="text-danger">{{ $message }}</span>
                                    @enderror

                                </div>

                            <div class="input-group input-group-static mb-4">
                                <label for="exampleFormControlSelect1" name="group" class="ms-0">Group</label>
                                <select class="form-control" id="exampleFormControlSelect1">
                                    <option value="system">System</option>
                                    <option value="user">User</option>

                                </select>
                                @error('group')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                                <div class="form-group">
                                    <label for="">Permission</label>
                                    <div class="row">
                                        @foreach($permission as $group_name => $values)
                                            <div class="col-4">

                                                <h4> {{ $group_name }}</h4>

                                                @foreach($values as $value)
                                                    <div class="form-check">
                                                        <input id="box-{{ $value->id }}" class="form-check-input" name="permission_id[]" value="{{ $value->id }}" type="checkbox"
                                                            {{ (is_array(old("permission_id")) and in_array($value->id, old("permission_id"))) ? 'checked' : '' }}/>
                                                        <label class="custom-control-label"  for="customCheck1" {{ $value->id }}">{{ $value->display_name }}</label>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                                <button type="submit" class="btn btn-submit btn-primary">Create</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
