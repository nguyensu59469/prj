@extends("admin.Layouts.dashboard")
{{--@extends("layouts.app")--}}
@section("content")
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-md-4" style="margin: auto;padding: 10px;text-align: center">
                <h2 class="page-title">Detail role {{ $role->name }}</h2>

            </div>
        </div>
        <!-- end page title -->

        <div class="row">
            <div class="col-md-6" style="margin: auto">
                <div class="card">
                    <div class="card-body">
                        <dl class="row">
                            <dt class="col-sm-4">Id :</dt>
                            <dd class="col-sm-8">{{ $role->id }}</dd>
                            <dt class="col-sm-4">Name :</dt>
                            <dd class="col-sm-8">{{ $role->name }}</dd>
                            <dt class="col-sm-4">Display name :</dt>
                            <dd class="col-sm-8">{{ $role->display_name }}</dd>
                            @if($role->name != "super-admin")
                                <dt class="col-sm-4">Permission :</dt>
                                <dd class="col-sm-8">
                                    <div class="row">
                                        @foreach($role->permissions as $item)
                                            <div class="col-sm-4">
                                                <span class="badge bg-success">{{ $item->display_name }}</span>
                                            </div>
                                        @endforeach
                                    </div>
                                </dd>
                            @endif
                        </dl>
                    </div>

                </div><br>
                <div>
                    <a type="button" href="{{ url()->previous() }}" class="btn btn-primary btn-button">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
