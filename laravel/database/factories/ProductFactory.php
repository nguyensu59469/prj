<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [

            'name' => $this->faker->name(),
            'price' => $this->faker->numberBetween($min = 10, $max = 9000),
            'image' => $this->faker->image(),
            'quantity' => $this->faker->quantity(),
            'description' => $this->faker->text(),
        ];
    }
}
